#include <string>
#include <iostream>
#include <math.h>

std::string getFibString(std::string A, std::string B, int index);
int summation(std::string A, std::string B, int n);

int main(){
    const std::string A = "1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679";
    const std::string B = "8214808651328230664709384460955058223172535940812848111745028410270193852110555964462294895493038196"; 

    std::cout << summation(A, B, 17);
}

std::string getFibString(std::string A, std::string B, int index){
    std::string x = B;
    std::string y = A+B;
    std::string next;
    int count{0};

    while ((x.length() < index)){
        next = x + y;
        x = y;
        y = next;
    }
    return x;
}

int summation(std::string A, std::string B, int n){
    double sum{0.0};
    for (int i = 0; i <= n; i++){
        double index{(127 + (19 * i)) * pow(7, i)};
        sum += (pow(10, i) * getFibString(A, B, index - 1).at(index - 1));
    }
    return sum;
}
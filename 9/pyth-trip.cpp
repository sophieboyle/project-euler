#include <iostream>

int compute();
inline int computeProd(int a, int b, int c);
inline int computeSum(int a, int b, int c);
bool isPythagoras(int a, int b, int c);

int main(){
    std::cout << compute() << '\n';
}

int compute(){
    int c{0};
    for (int a = 1; a < 1000; ++a){
        for (int b = 1; b < 1000; ++b){
            c = 1000 - a - b;
            if (isPythagoras(a, b, c) && (computeSum(a, b, c) == 1000)){
                return computeProd(a, b, c);
            }
        }
    }
    return 0;
}

inline int computeProd(int a, int b, int c){
    return a * b * c;
}

inline int computeSum(int a, int b, int c){
    return a + b + c;
}

bool isPythagoras(int a, int b, int c){
    if ((a * a) + (b * b) == (c * c)){
        return true;
    } else {
        return false;
    }
}
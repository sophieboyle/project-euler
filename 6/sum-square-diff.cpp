#include <iostream>

long sumAndSquareDiff(int x);

int main(){
    std::cout << sumAndSquareDiff(100) << '\n';
}

long sumAndSquareDiff(int x){
    long sum{0};
    long squares{0};
    for (int i = 1; i <= x; ++i){
        sum += i;
        squares += i * i;
    }
    return (sum * sum) - squares;
}
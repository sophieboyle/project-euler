#include <iostream>

int fib(int a);

int main(){
    long sum{0};
    int n{0};

    for (int i = 0; i == i; i++){
        n = fib(i);
        if (n >= 4000000){
            break;
        } else if ((n % 2) == 0){
            sum += n;
        }
    }

    std::cout << sum << '\n';
}

int fib(int a){
    if (a < 2){
        return a;
    } else {
        return fib(a - 1) + fib(a - 2);
    }
}
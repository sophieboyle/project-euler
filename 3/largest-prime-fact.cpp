#include <iostream>

int getLargestPrime(long x);

int main(){
    std::cout << getLargestPrime(600851475143) << '\n';
}

int getLargestPrime(long x){
    for (long y = 2; y < x; y++){
        if ((x % y) == 0){
            x = x / y;
        }
    }
    return x;
}

#include <iostream>

int getPrime(int x);
bool isPrime(int x);

int main(){
    std::cout << getPrime(10001) << '\n';
}

int getPrime(int x){
    int primeth{0};
    int latestPrime{0};
    int i{2};
    while (primeth != x){
        if (isPrime(i)){
            ++primeth;
            latestPrime = i;
        }
        ++i;
    }
    return latestPrime;
}

bool isPrime(int x){
    for (int y = x - 1; y > 1; --y){
        if ((x % y) == 0){
            return false;
        }
    }
    return true;
}
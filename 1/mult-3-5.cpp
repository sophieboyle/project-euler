#include <iostream>

int sum(int n);

int main(){
    std::cout << sum(1000);
}

int sum(int n){
    int sum{0};
    for (int i = 1; i < n; i++){
        if (((i%3)==0) || ((i%5)==0)){
            sum += i;
        }
    }
    return sum;
}
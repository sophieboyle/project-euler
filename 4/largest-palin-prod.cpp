#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>

bool isPalindrome(std::string x);
int compute();
std::string convert(int x);

int main(){
    std::cout << compute() << '\n';
}

bool isPalindrome(std::string x){
    std::string y = x;
    std::reverse(y.begin(), y.end());
    
    if (x == y){
        return true;
    } else {
        return false;
    }
}

int compute(){
    int z{999};
    int prod{0};
    int largest{0};

    for (int x = z; x >= 100; x--){
        for (int y = z; y >= 100; y--){
            prod = x * y;

            if (isPalindrome(convert(prod))){
                if (prod > largest) {
                    largest = prod;
                }
            }
        }
    }
    return largest;
}

std::string convert(int x){
    std::ostringstream y;
    y << x;
    std::string result = y.str();
    return result;
}